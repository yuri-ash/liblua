-- A project defines one build target
project "Lua"
	location ( "build/" .. action )
	kind "StaticLib"
	language "C++"
	includedirs {
		"include/lua",
	}
	files {
		"include/lua/**.h","src/**.c"
	}
	excludes { "src/luac.c", "src/lua.c" }
	
	configuration { "vs2010 or vs2013"}
		buildoptions { "/wd4996" }
		flags { "NoMinimalRebuild", "FatalWarnings"}
	configuration{}
